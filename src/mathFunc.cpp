#include <math.h>
#include "MainUnit.h"

double result(int operation, double num1, double num2 = 0){
    switch(operation){
        case 1: return num1 + num2;
        case 2: return num1 - num2;
        case 3: return num1 * num2;
        case 4: return num1/num2;
		case 5: return sqrt(num1);
		case 6: return pow(num1, num2);
		case 7: return sin(num1);
		case 8: return cos(num1);
		case 9: return tan(num1);
		case 10: return 1/tan(num1);
		case 11: return 1/num1;
		case 12: return asin(num1);
		case 13: return acos(num1);
		case 14: return atan(num1);
		case 15: return M_PI_2 - atan(num1);
    }
}

double returnNumber(bool &firstNumber, UnicodeString NumberField, double number1, double number2, int operation){
    switch(firstNumber){
        case true:{
            firstNumber = false;
            number1 = StrToFloat(NumberField);
            return number1;
        }
        default:{
            number2 = StrToFloat(NumberField);
            return result(operation, number1, number2);
        }
    }
}


