#include <vcl.h>
#include "MainUnit.h"
#include "Unit2.h"
#include "Unit3.h"
#include "src/src.h"

#pragma package(smart_init)
#pragma resource "*.dfm"
#pragma hdrstop

#define rowCount1 Form1 -> matrix1 -> RowCount
#define rowCount2 Form1 -> matrix2 -> RowCount
#define colCount1 Form1 -> matrix1 -> ColCount
#define colCount2 Form1 -> matrix2 -> ColCount

TForm1 *Form1;
UnicodeString NumberField;
UnicodeString toMemo = "";

bool   firstNumber = true;
bool   matrixEnabled = false;
double number1 = 0;
double number2 = 0;
int    operation = 1;
int    equaltyCount = 1;
int    histIter = 0;
double memoryNumber = 0;

/*
	���� ��������:
	1 -> ��������
	2 -> ���������
	3 -> ���������
	4 -> �������
	5 -> ������
	6 -> ���������� � ������������ �������
	7 -> �����
	8 -> �������
	9 -> �������
	10 -> ���������
	11 -> ���������� ����� � -1 �������
	12 -> ��������
	13 -> ����������
	14 -> ����������
	15 -> ������������
*/
__fastcall TForm1::TForm1(TComponent* Owner)
    : TForm(Owner)
{

}

//���� =========================================================================
void __fastcall TForm1::numPad1Click(TObject *Sender)
{
	NumberField = NumberField + IntToStr(1);
	Edit1->Text = NumberField;
    equaltyBut->SetFocus();
}
void __fastcall TForm1::numPad2Click(TObject *Sender)
{
    NumberField = NumberField + IntToStr(2);
	Edit1->Text = NumberField;
	equaltyBut->SetFocus();
}

void __fastcall TForm1::numPad3Click(TObject *Sender)
{
    NumberField = NumberField + IntToStr(3);
	Edit1->Text = NumberField;
	equaltyBut->SetFocus();
}

void __fastcall TForm1::numPad4Click(TObject *Sender)
{
    NumberField = NumberField + IntToStr(4);
	Edit1->Text = NumberField;
	equaltyBut->SetFocus();
}

void __fastcall TForm1::numPad5Click(TObject *Sender)
{
    NumberField = NumberField + IntToStr(5);
	Edit1->Text = NumberField;
	equaltyBut->SetFocus();
}

void __fastcall TForm1::numPad6Click(TObject *Sender)
{
    NumberField = NumberField + IntToStr(6);
	Edit1->Text = NumberField;
	equaltyBut->SetFocus();
}

void __fastcall TForm1::numPad7Click(TObject *Sender)
{
    NumberField = NumberField + IntToStr(7);
	Edit1->Text = NumberField;
	equaltyBut->SetFocus();
}

void __fastcall TForm1::numPad8Click(TObject *Sender)
{
    NumberField = NumberField + IntToStr(8);
	Edit1->Text = NumberField;
	equaltyBut->SetFocus();
}

void __fastcall TForm1::numPad9Click(TObject *Sender)
{
    NumberField = NumberField + IntToStr(9);
	Edit1->Text = NumberField;
	equaltyBut->SetFocus();
}

void __fastcall TForm1::numPad0Click(TObject *Sender)
{
    NumberField = NumberField + IntToStr(0);
	Edit1->Text = NumberField;
	equaltyBut->SetFocus();
}

void __fastcall TForm1::numPadSepClick(TObject *Sender)
{
    NumberField = NumberField + ',';
    Edit1->Text = NumberField;
	numPadSep->Enabled = false;
	equaltyBut->SetFocus();
}
//==============================================================================

//��������
void __fastcall TForm1::plusButClick(TObject *Sender)
{
    if(operation == 4 && StrToFloat(NumberField) == 0){
        errorZero();
        return;
    }

    NumberField = Edit1->Text;

    number1 = returnNumber(firstNumber, NumberField, number1, number2, operation);
    operation = 1;
	toMemo += NumberField + "+";

	toWindow();
	equaltyBut->SetFocus();
}

//���������
void __fastcall TForm1::minusButClick(TObject *Sender)
{
    if(operation == 4 && StrToFloat(NumberField) == 0){
        errorZero();
        return;
    }

    NumberField = Edit1->Text;
    number1 = returnNumber(firstNumber, NumberField, number1, number2, operation);
    operation = 2;
	toMemo += NumberField + "-";

	toWindow();
	equaltyBut->SetFocus();
}

//���������
void __fastcall TForm1::equaltyButClick(TObject *Sender)
{
    if(operation == 4 && StrToFloat(NumberField) == 0){
        errorZero();
        return;
    }

    NumberField = Edit1->Text;
    number2 = StrToFloat(NumberField);
    number1 = result(operation, number1, number2);
    toMemoHistory();
    toWindow();
	firstNumber = true;
	equaltyBut->SetFocus();
}

//���������
void __fastcall TForm1::multipleButClick(TObject *Sender)
{
    if(operation == 4 && StrToFloat(NumberField) == 0){
        errorZero();
        return;
    }

    NumberField = Edit1->Text;
    number1 = returnNumber(firstNumber, NumberField, number1, number2, operation);
    operation = 3;
    toMemo = "(" + toMemo + NumberField + ")*";

	toWindow();
	equaltyBut->SetFocus();
}

//�������
void __fastcall TForm1::divideButClick(TObject *Sender)
{
    if(operation == 4 && StrToFloat(NumberField) == 0){
        errorZero();
        return;
    }

	NumberField = Edit1->Text;
	number1 = returnNumber(firstNumber, NumberField, number1, number2, operation);
    operation = 4;
    toMemo = "(" + toMemo + NumberField + ")/";

	toWindow();
	equaltyBut->SetFocus();
}

//������
void __fastcall TForm1::sqrtButClick(TObject *Sender)
{
    if(StrToFloat(Edit1->Text) < 0){
        errorMessageSqrt();
        endOfOperation();
        Edit1->Text = "������!";
        return;
    }

	UnicodeString numBufMemo = Edit1->Text;
	number1 = returnNumber(firstNumber, Edit1->Text, number1, number2, operation);
	operation = 5;
	Edit1->Text = result(operation, number1);
	NumberField = Edit1->Text;

	if(firstNumber == true) toMemo = "sqrt(" + FloatToStr(number1) + ")=" + NumberField;
	else toMemo = "sqrt(" + toMemo + numBufMemo + ")=" + NumberField;

	toMemoHistory();
    toWindow();
	numPadSep->Enabled = true;
	firstNumber = true;
    operation = 1;
	equaltyBut->SetFocus();
}

//����� �����
void __fastcall TForm1::changeButClick(TObject *Sender)
{
    NumberField = FloatToStr((-1)*StrToFloat(NumberField));
    Edit1->Text = NumberField;
	numPadSep->Enabled = true;
	equaltyBut->SetFocus();
}

//� -1 �������
void __fastcall TForm1::minusOneButClick(TObject *Sender)
{

	if(StrToFloat(Edit1->Text) == 0){
		errorMessageM();
		endOfOperation();
		Edit1->Text = "������!";
		return;
    }

	UnicodeString numBufMemo = Edit1->Text;
	number1 = returnNumber(firstNumber, Edit1->Text, number1, number2, operation);
	operation = 11;
	Edit1->Text = result(operation, number1);
	NumberField = Edit1->Text;

	if(firstNumber == true) toMemo = "(" + FloatToStr(number1) + ")^-1=" + NumberField;
	else toMemo = "(" + toMemo + numBufMemo + ")^-1=" + NumberField;

	toMemoHistory();
	numPadSep->Enabled = true;
	equaltyBut->SetFocus();
}

//������� ��������
void __fastcall TForm1::clearElementClick(TObject *Sender)
{
	clearAction();
	equaltyBut->SetFocus();
}

//������� �������
void __fastcall TForm1::clearAllClick(TObject *Sender)
{
    clearAction();
    number1 = 0;
	firstNumber = true;
	toMemo = "";
	equaltyBut->SetFocus();
}

void __fastcall TForm1::N5Click(TObject *Sender)
{
    Help->Show();
	equaltyBut->SetFocus();
}

//�������
void __fastcall TForm1::N3Click(TObject *Sender)
{
	Form2->Show();
	equaltyBut->SetFocus();
}

//��������� ����� � ������
void __fastcall TForm1::memoryInClick(TObject *Sender)
{
    if(NumberField == "") NumberField = FloatToStr(number1);
    memoryNumber = StrToFloat(NumberField);
    Edit2->Text = FloatToStr(memoryNumber);
    memoryIn->Enabled = false;
    memoryClear->Enabled = true;
    memoryRecover->Enabled = true;
    memoryMinus->Enabled = true;
	memoryPlus->Enabled = true;
	equaltyBut->SetFocus();
}

//������� ������
void __fastcall TForm1::memoryClearClick(TObject *Sender)
{
    memoryNumber = 0;
    Edit2->Text = FloatToStr(memoryNumber);
    memoryIn->Enabled = true;
    memoryClear->Enabled = false;
    memoryRecover->Enabled = false;
    memoryMinus->Enabled = false;
	memoryPlus->Enabled = false;
	equaltyBut->SetFocus();
}

//����� ����� �� ������
void __fastcall TForm1::memoryRecoverClick(TObject *Sender)
{
    Edit1->Text = FloatToStr(memoryNumber);
	NumberField = FloatToStr(memoryNumber);
	equaltyBut->SetFocus();
}

//��������� � ����� � ������ ����� �� �������� ����
void __fastcall TForm1::memoryMinusClick(TObject *Sender)
{
    memoryNumber -= StrToFloat(Edit1->Text);
	Edit2->Text = FloatToStr(memoryNumber);
	equaltyBut->SetFocus();
}

//������� �� ����� � ������ ����� �� �������� ����
void __fastcall TForm1::memoryPlusClick(TObject *Sender)
{
	memoryNumber += StrToFloat(Edit1->Text);
	Edit2->Text = FloatToStr(memoryNumber);
	equaltyBut->SetFocus();
}

//���������� � �������
void __fastcall TForm1::tteButClick(TObject *Sender)
{
    operation = 6;
	number1 = returnNumber(firstNumber, Edit1->Text, number1, number2, operation);
	UnicodeString numBufMemo = FloatToStr(number1);
	number2 = StrToFloat(Edit3->Text);
	if(number1 == 0 && number2 < 0){
		errorMessageM();
		endOfOperation();
		Edit1->Text = "������!";
		Edit3->Text = "0";
		return;
	}
	number1 = result(operation, number1, number2);
	if(firstNumber == true) toMemo = "(" + numBufMemo + ")^" + Edit3->Text + "=" + FloatToStr(number1);
	else toMemo = "(" + toMemo + Edit1->Text + ")^" + Edit3->Text + "=" + FloatToStr(number1);

	toMemoHistory();
	Edit3->Text = "0";
	toWindow();
	operation = 1;
    firstNumber = true;
	equaltyBut->SetFocus();
}

//�����
void __fastcall TForm1::sinButClick(TObject *Sender)
{
	NumberField = Edit1->Text;
	number1 = returnNumber(firstNumber, Edit1->Text, number1, number2, operation);
	UnicodeString numBufMemo = FloatToStr(number1);

	operation = 7;
	number1 = result(operation, number1, number2);

	if(firstNumber == true) toMemo = "sin(" + numBufMemo + ")=" + FloatToStr(number1);
	else toMemo = "sin(" + toMemo + Edit1->Text + ")=" + FloatToStr(number1);

	toMemoHistory();
	toWindow();
	equaltyBut->SetFocus();
}

//�������
void __fastcall TForm1::cosButClick(TObject *Sender)
{
	NumberField = Edit1->Text;
	number1 = returnNumber(firstNumber, Edit1->Text, number1, number2, operation);
	UnicodeString numBufMemo = FloatToStr(number1);

	operation = 8;
	number1 = result(operation, number1, number2);

	if(firstNumber == true) toMemo = "cos(" + numBufMemo + ")=" + FloatToStr(number1);
	else toMemo = "cos(" + toMemo + Edit1->Text + ")=" + FloatToStr(number1);

	toMemoHistory();
	toWindow();
	equaltyBut->SetFocus();
}

//�������
void __fastcall TForm1::tgButClick(TObject *Sender)
{
	NumberField = Edit1->Text;
	number1 = returnNumber(firstNumber, Edit1->Text, number1, number2, operation);
	UnicodeString numBufMemo = FloatToStr(number1);

	operation = 9;
	number1 = result(operation, number1, number2);

	if(firstNumber == true) toMemo = "tg(" + numBufMemo + ")=" + FloatToStr(number1);
	else toMemo = "tg(" + toMemo + Edit1->Text + ")=" + FloatToStr(number1);

	toMemoHistory();
	toWindow();
	equaltyBut->SetFocus();
}

//���������
void __fastcall TForm1::ctgButClick(TObject *Sender)
{
	NumberField = Edit1->Text;
	number1 = returnNumber(firstNumber, Edit1->Text, number1, number2, operation);
	UnicodeString numBufMemo = FloatToStr(number1);

	operation = 10;
	number1 = result(operation, number1, number2);

	if(firstNumber == true) toMemo = "ctg(" + numBufMemo + ")=" + FloatToStr(number1);
	else toMemo = "ctg(" + toMemo + Edit1->Text + ")=" + FloatToStr(number1);

	toMemoHistory();
	toWindow();
	equaltyBut->SetFocus();
}

//��������
void __fastcall TForm1::arcSinButClick(TObject *Sender)
{
	NumberField = Edit1->Text;
	number1 = returnNumber(firstNumber, Edit1->Text, number1, number2, operation);
	UnicodeString numBufMemo = FloatToStr(number1);

	operation = 12;
	number1 = result(operation, number1, number2);

	if(firstNumber == true) toMemo = "arcsin(" + numBufMemo + ")=" + FloatToStr(number1);
	else toMemo = "arcsin(" + toMemo + Edit1->Text + ")=" + FloatToStr(number1);

	toMemoHistory();
	toWindow();
	equaltyBut->SetFocus();
}

//����������
void __fastcall TForm1::arcCosButClick(TObject *Sender)
{
	NumberField = Edit1->Text;
	number1 = returnNumber(firstNumber, Edit1->Text, number1, number2, operation);
	UnicodeString numBufMemo = FloatToStr(number1);

	operation = 13;
	number1 = result(operation, number1, number2);

	if(firstNumber == true) toMemo = "arccos(" + numBufMemo + ")=" + FloatToStr(number1);
	else toMemo = "arccos(" + toMemo + Edit1->Text + ")=" + FloatToStr(number1);

	toMemoHistory();
	toWindow();
	equaltyBut->SetFocus();
}

//����������
void __fastcall TForm1::arcTanButClick(TObject *Sender)
{   NumberField = Edit1->Text;
	number1 = returnNumber(firstNumber, Edit1->Text, number1, number2, operation);
	UnicodeString numBufMemo = FloatToStr(number1);

	operation = 14;
	number1 = result(operation, number1, number2);

	if(firstNumber == true) toMemo = "arctg(" + numBufMemo + ")=" + FloatToStr(number1);
	else toMemo = "arctg(" + toMemo + Edit1->Text + ")=" + FloatToStr(number1);

	toMemoHistory();
	toWindow();
	equaltyBut->SetFocus();
}

//������������
void __fastcall TForm1::arcCtgButClick(TObject *Sender)
{
	NumberField = Edit1->Text;
	number1 = returnNumber(firstNumber, Edit1->Text, number1, number2, operation);
	UnicodeString numBufMemo = FloatToStr(number1);

	operation = 15;
	number1 = result(operation, number1, number2);

	if(firstNumber == true) toMemo = "arcctg(" + numBufMemo + ")=" + FloatToStr(number1);
	else toMemo = "arcctg(" + toMemo + Edit1->Text + ")=" + FloatToStr(number1);

	toMemoHistory();
	toWindow();
	equaltyBut->SetFocus();
}

//����� ��������
void TForm1::endOfOperation(){
	operation = 1;
	NumberField = "";
	numPadSep->Enabled = true;
}

//������� �������
void TForm1::clearAction(){
	NumberField = "";
	Edit1->Text = "0";
	numPadSep->Enabled = true;
}

//����� ����� � ������� ����
void TForm1::toWindow(){
	Edit1->Text = FloatToStr(number1);
	NumberField = "";
	numPadSep->Enabled = true;
}

//����� ������� � �������
void TForm1::toMemoHistory(){
	switch(operation){
		case 1: toMemo += NumberField + "=" + FloatToStr(number1); break;
		case 2: toMemo += NumberField + "=" + FloatToStr(number1); break;
		case 3: toMemo += NumberField + "=" + FloatToStr(number1); break;
		case 4: toMemo += NumberField + "=" + FloatToStr(number1); break;
		default:;
	}
	History->Lines->Append(IntToStr(equaltyCount) + ") " + toMemo);
	toMemo = "";
	++equaltyCount;
}

//����� ������ ��� ������� �� 0
void TForm1::errorZero(){
	errorMessageZero();
	endOfOperation();
	Edit1->Text = "������!";
	firstNumber = true;
}


void __fastcall TForm1::N4Click(TObject *Sender)
{
	if(N4->Checked == true){
		Form1->ClientHeight = 249;
		arcCosBut->Visible = false;
		arcSinBut->Visible = false;
		arcTanBut->Visible = false;
		arcCtgBut->Visible = false;
		sinBut->Visible = false;
		tgBut->Visible = false;
		cosBut->Visible = false;
		ctgBut->Visible = false;
		trign->Visible = false;
		N4->Checked = false;
	}
	else{
		Form1->ClientHeight = 300;
		arcCosBut->Visible = true;
		arcSinBut->Visible = true;
		arcTanBut->Visible = true;
		arcCtgBut->Visible = true;
		sinBut->Visible = true;
		tgBut->Visible = true;
		cosBut->Visible = true;
		ctgBut->Visible = true;
		trign->Visible = true;
		N4->Checked = true;
	}
	equaltyBut->SetFocus();
}
void __fastcall TForm1::buttonMatrixCalcEnableClick(TObject *Sender)
{
    if (matrixEnabled)
	{
        buttonIncreaseRows1        -> Visible = false;
        buttonDecreaseRows1        -> Visible = false;
        buttonIncreaseCols1        -> Visible = false;
        labelColCount1             -> Visible = false;
        buttonDecreaseCols1        -> Visible = false;
        buttonIncreaseRows2        -> Visible = false;
		labelRowCount2             -> Visible = false;
        buttonDecreaseRows2        -> Visible = false;
        buttonIncreaseCols2        -> Visible = false;
        buttonDecreaseCols2        -> Visible = false;
        calcDet1Result             -> Visible = false;
        ������������               -> Visible = false;
        labelRowCount1             -> Visible = false;
		labelColCount2             -> Visible = false;
        matrix1                    -> Visible = false;
        matrix2                    -> Visible = false;
        matrixResult               -> Visible = false;
        buttonMatrixAddition       -> Visible = false;
        buttonMatrixSubstraction   -> Visible = false;
        labelMatrixes              -> Visible = false;
		buttonMatrixMultiplication -> Visible = false;
        buttonMatrix1Det           -> Visible = false;

        matrixEnabled = false;
    }
    else
    {
		buttonIncreaseRows1        -> Visible = true;
        buttonDecreaseRows1        -> Visible = true;
        buttonIncreaseCols1        -> Visible = true;
        labelColCount1             -> Visible = true;
        buttonDecreaseCols1        -> Visible = true;
        buttonIncreaseRows2        -> Visible = true;
        labelRowCount2             -> Visible = true;
		buttonDecreaseRows2        -> Visible = true;
        buttonIncreaseCols2        -> Visible = true;
        buttonDecreaseCols2        -> Visible = true;
        calcDet1Result             -> Visible = true;
        ������������               -> Visible = true;
        labelRowCount1             -> Visible = true;
        labelColCount2             -> Visible = true;
		matrix1                    -> Visible = true;
        matrix2                    -> Visible = true;
        matrixResult               -> Visible = true;
        buttonMatrixAddition       -> Visible = true;
        buttonMatrixSubstraction   -> Visible = true;
        labelMatrixes              -> Visible = true;
        buttonMatrixMultiplication -> Visible = true;
		buttonMatrix1Det           -> Visible = true;

        matrixEnabled = true;
	}
	equaltyBut->SetFocus();
}

// ��������� �����������

void __fastcall TForm1::buttonIncreaseRows1Click(TObject *Sender)
{
	matrix1 -> RowCount++;
	equaltyBut->SetFocus();
}

void __fastcall TForm1::buttonDecreaseRows1Click(TObject *Sender)
{
	matrix1 -> RowCount--;
	equaltyBut->SetFocus();
}

void __fastcall TForm1::buttonIncreaseCols1Click(TObject *Sender)
{
	matrix1 -> ColCount++;
	equaltyBut->SetFocus();
}

void __fastcall TForm1::buttonDecreaseCols1Click(TObject *Sender)
{
	matrix1 -> ColCount--;
	equaltyBut->SetFocus();
}

void __fastcall TForm1::buttonIncreaseRows2Click(TObject *Sender)
{
	matrix2 -> RowCount++;
	equaltyBut->SetFocus();
}

void __fastcall TForm1::buttonDecreaseRows2Click(TObject *Sender)
{
	matrix2 -> RowCount--;
	equaltyBut->SetFocus();
}

void __fastcall TForm1::buttonIncreaseCols2Click(TObject *Sender)
{
	matrix2 -> ColCount++;
	equaltyBut->SetFocus();
}
void __fastcall TForm1::buttonDecreaseCols2Click(TObject *Sender)
{
	matrix2 -> ColCount--;
	equaltyBut->SetFocus();
}

void __fastcall TForm1::buttonMatrixAdditionClick(TObject *Sender)
{
	if (!checkMatrix1 || !checkMatrix2)
		return;
    else if ( rowCount1 != rowCount2 || colCount1 != colCount2 )
    {
        Dialogs::ShowMessage("������ �����������");
        return;
	}
	matrixAddition();
	equaltyBut->SetFocus();
}

void __fastcall TForm1::buttonMatrixSubstractionClick(TObject *Sender)
{
	if (!checkMatrix1 || !checkMatrix2)
		return;
	matrixSubstraction();
	equaltyBut->SetFocus();
}
void __fastcall TForm1::buttonMatrix1DetClick(TObject *Sender)
{
	if (matrix1 -> RowCount != matrix1 -> ColCount )
	{
		Dialogs::ShowMessage("������ �����������.");
		return;
	}
	if ( rowCount1 != rowCount2 || colCount1 != colCount2 )
	{
		Dialogs::ShowMessage("������ �����������");
		return;
	}

	double det = calcDet();
	calcDet1Result -> Text = det;
	equaltyBut->SetFocus();
}
//---------------------------------------------------------------------------

void __fastcall TForm1::buttonMatrixMultiplicationClick(TObject *Sender)
{
	if ( colCount1 != rowCount2 )
	{
			Dialogs::ShowMessage("������ �����������");
			return;
	}
	matrixMultiplication();
    equaltyBut->SetFocus();
}
void __fastcall TForm1::FormKeyDown(TObject *Sender, WORD &Key, TShiftState Shift)

{
	switch(Key){
		case 8: clearElementClick(clearElement); break;
		case 13: equaltyButClick(equaltyBut); break;
		case 67: clearAllClick(clearAll); break;
		case 68: memoryClearClick(memoryClear); break;
		case 69: tteButClick(tteBut); break;
		case 73: memoryInClick(memoryIn); break;
		case 78: minusOneButClick(minusOneBut); break;
		case 79: changeButClick(changeBut); break;
		case 82: memoryRecoverClick(memoryRecover); break;
        case 83: sqrtButClick(sqrtBut); break;
		case 96: numPad0Click(numPad0); break;
		case 97: numPad1Click(numPad1); break;
		case 98: numPad2Click(numPad2); break;
		case 99: numPad3Click(numPad3); break;
		case 100: numPad4Click(numPad4); break;
		case 101: numPad5Click(numPad5); break;
		case 102: numPad6Click(numPad6); break;
		case 103: numPad7Click(numPad7); break;
		case 104: numPad8Click(numPad8); break;
		case 105: numPad9Click(numPad9); break;
		case 106: multipleButClick(multipleBut); break;
		case 107: plusButClick(plusBut); break;
		case 108: minusButClick(minusBut); break;
		case 110: divideButClick(divideBut); break;
		case 112: N5Click(N5); break;
		case 187: equaltyButClick(equaltyBut); break;
		case 188: numPadSepClick(numPadSep); break;
        case VK_F2: N3Click(N3); break;
        case VK_F3: N4Click(N3); break;
        case VK_F4: buttonMatrixCalcEnableClick(buttonMatrixCalcEnable); break;
		default:;
	}

}
//---------------------------------------------------------------------------

void __fastcall TForm1::Edit3Enter(TObject *Sender)
{
	Form1->KeyPreview = false;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Edit3Exit(TObject *Sender)
{
	Form1->KeyPreview = true;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::matrix1Enter(TObject *Sender)
{
	Form1->KeyPreview = false;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::matrix1Exit(TObject *Sender)
{
    Form1->KeyPreview = true;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::matrix2Enter(TObject *Sender)
{
	Form1->KeyPreview = false;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::matrix2Exit(TObject *Sender)
{
	Form1->KeyPreview = true;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::matrixResultEnter(TObject *Sender)
{
	Form1->KeyPreview = false;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::matrixResultExit(TObject *Sender)
{
	Form1->KeyPreview = true;
}
