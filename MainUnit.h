//---------------------------------------------------------------------------

#ifndef MainUnitH
#define MainUnitH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.ComCtrls.hpp>
#include <Vcl.ToolWin.hpp>
#include <Vcl.Menus.hpp>
#include <Vcl.Grids.hpp>
//---------------------------------------------------------------------------
class TForm1 : public TForm
{
__published:	// IDE-managed Components
    TEdit *Edit1;
    TButton *numPad1;
    TButton *numPad2;
    TButton *numPad3;
    TButton *numPad4;
    TButton *numPad5;
    TButton *numPad6;
    TButton *numPad7;
    TButton *numPad8;
    TButton *numPad9;
    TButton *numPad0;
    TButton *numPadSep;
    TButton *plusBut;
    TButton *equaltyBut;
    TButton *minusBut;
    TMemo *History;
    TButton *multipleBut;
    TButton *divideBut;
    TButton *sqrtBut;
    TButton *minusOneBut;
    TButton *clearAll;
    TButton *changeBut;
    TButton *clearElement;
    TMainMenu *MainMenu1;
    TMenuItem *N1;
    TMenuItem *N2;
    TMenuItem *N3;
    TEdit *Edit2;
    TButton *memoryPlus;
    TButton *memoryMinus;
    TButton *memoryRecover;
    TButton *memoryClear;
    TButton *memoryIn;
	TButton *tteBut;
	TEdit *Edit3;
	TMenuItem *N4;
	TButton *sinBut;
	TButton *cosBut;
	TButton *tgBut;
	TButton *ctgBut;
	TButton *arcSinBut;
	TButton *arcCosBut;
	TButton *arcTanBut;
	TButton *arcCtgBut;
	TStaticText *trign;
    TStringGrid *matrix1;
    TStringGrid *matrix2;
    TStringGrid *matrixResult;
    TButton *buttonMatrixAddition;
    TButton *buttonMatrixSubstraction;
    TLabel *labelMatrixes;
    TButton *buttonMatrixMultiplication;
    TButton *buttonMatrix1Det;
    TMenuItem *buttonMatrixCalcEnable;
    TButton *buttonIncreaseRows1;
    TButton *buttonDecreaseRows1;
    TButton *buttonIncreaseCols1;
    TLabel *labelColCount1;
    TButton *buttonDecreaseCols1;
    TButton *buttonIncreaseRows2;
    TLabel *labelRowCount2;
    TButton *buttonDecreaseRows2;
    TButton *buttonIncreaseCols2;
    TButton *buttonDecreaseCols2;
    TEdit *calcDet1Result;
    TLabel *������������;
    TLabel *labelRowCount1;
    TLabel *labelColCount2;
	TMenuItem *N5;
    void __fastcall numPad1Click(TObject *Sender);
    void __fastcall numPad2Click(TObject *Sender);
    void __fastcall numPad3Click(TObject *Sender);
    void __fastcall numPad4Click(TObject *Sender);
    void __fastcall numPad5Click(TObject *Sender);
    void __fastcall numPad6Click(TObject *Sender);
    void __fastcall numPad7Click(TObject *Sender);
    void __fastcall numPad8Click(TObject *Sender);
    void __fastcall numPad9Click(TObject *Sender);
    void __fastcall numPad0Click(TObject *Sender);
    void __fastcall numPadSepClick(TObject *Sender);
    void __fastcall plusButClick(TObject *Sender);
    void __fastcall equaltyButClick(TObject *Sender);
    void __fastcall minusButClick(TObject *Sender);
    void __fastcall multipleButClick(TObject *Sender);
    void __fastcall divideButClick(TObject *Sender);
    void __fastcall sqrtButClick(TObject *Sender);
    void __fastcall changeButClick(TObject *Sender);
    void __fastcall minusOneButClick(TObject *Sender);
    void __fastcall clearElementClick(TObject *Sender);
    void __fastcall clearAllClick(TObject *Sender);
    void __fastcall N3Click(TObject *Sender);
    void __fastcall memoryInClick(TObject *Sender);
    void __fastcall memoryClearClick(TObject *Sender);
    void __fastcall memoryRecoverClick(TObject *Sender);
    void __fastcall memoryMinusClick(TObject *Sender);
    void __fastcall memoryPlusClick(TObject *Sender);
	void __fastcall tteButClick(TObject *Sender);
	void __fastcall N4Click(TObject *Sender);
	void __fastcall sinButClick(TObject *Sender);
	void __fastcall cosButClick(TObject *Sender);
	void __fastcall tgButClick(TObject *Sender);
	void __fastcall ctgButClick(TObject *Sender);
	void __fastcall arcSinButClick(TObject *Sender);
	void __fastcall arcCosButClick(TObject *Sender);
	void __fastcall arcTanButClick(TObject *Sender);
	void __fastcall arcCtgButClick(TObject *Sender);
    void __fastcall buttonMatrixCalcEnableClick(TObject *Sender);
    void __fastcall buttonIncreaseRows1Click(TObject *Sender);
    void __fastcall buttonDecreaseRows1Click(TObject *Sender);
    void __fastcall buttonIncreaseCols1Click(TObject *Sender);
    void __fastcall buttonDecreaseCols1Click(TObject *Sender);
    void __fastcall buttonIncreaseRows2Click(TObject *Sender);
    void __fastcall buttonDecreaseRows2Click(TObject *Sender);
    void __fastcall buttonIncreaseCols2Click(TObject *Sender);
    void __fastcall buttonDecreaseCols2Click(TObject *Sender);
    void __fastcall buttonMatrixAdditionClick(TObject *Sender);
    void __fastcall buttonMatrixSubstractionClick(TObject *Sender);
    void __fastcall buttonMatrix1DetClick(TObject *Sender);
	void __fastcall buttonMatrixMultiplicationClick(TObject *Sender);
	void __fastcall FormKeyDown(TObject *Sender, WORD &Key, TShiftState Shift);
	void __fastcall Edit3Enter(TObject *Sender);
	void __fastcall Edit3Exit(TObject *Sender);
	void __fastcall matrix1Enter(TObject *Sender);
	void __fastcall matrix1Exit(TObject *Sender);
	void __fastcall matrix2Enter(TObject *Sender);
	void __fastcall matrix2Exit(TObject *Sender);
	void __fastcall matrixResultEnter(TObject *Sender);
	void __fastcall matrixResultExit(TObject *Sender);
	void __fastcall N5Click(TObject *Sender);
private:    // User declarations
public:     // User declarations
    void toMemoHistory();
    void toWindow();
	void endOfOperation();
	void clearAction();
	void errorZero();
    __fastcall TForm1(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TForm1 *Form1;
//--------------------------------------------------------------------------
#endif
