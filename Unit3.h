//---------------------------------------------------------------------------

#ifndef Unit3H
#define Unit3H
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
//---------------------------------------------------------------------------
class THelp : public TForm
{
__published:	// IDE-managed Components
	TStaticText *StaticText1;
	TStaticText *StaticText2;
	TStaticText *StaticText3;
	TStaticText *StaticText4;
	TStaticText *StaticText5;
	TStaticText *StaticText6;
	TStaticText *StaticText7;
	TStaticText *StaticText8;
	TStaticText *StaticText9;
	TStaticText *StaticText10;
	TStaticText *StaticText11;
	TStaticText *StaticText12;
    TStaticText *StaticText13;
    TStaticText *StaticText14;
    TStaticText *StaticText15;
private:	// User declarations
public:		// User declarations
	__fastcall THelp(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE THelp *Help;
//---------------------------------------------------------------------------
#endif
