﻿#include <vcl.h>
#include "..\MainUnit.h"

#define rowCount1 Form1 -> matrix1      -> RowCount
#define rowCount2 Form1 -> matrix2      -> RowCount
#define colCount1 Form1 -> matrix1      -> ColCount
#define colCount2 Form1 -> matrix2      -> ColCount
#define rowCount3 Form1 -> matrixResult -> RowCount
#define colCount3 Form1 -> matrixResult -> ColCount

// Очистка третьей матрицы
void flushMatrix()
{
    for (int i = 0; i < rowCount3; i++)
    {
        for (int j = 0; j < colCount3; j++)
            Form1 -> matrixResult -> Cells[j][i] = "";
    }
}

// Считаем определитель матрицы
double calcDet()
{
    double determinant = 0;
    //UnicodeString **matrix = Form1 -> matrix1 -> Cells;

    if ( rowCount1 == 1 )
        determinant = StrToFloat(Form1 -> matrix1 -> Cells[0][0]);
    else if ( rowCount1 == 2 )
    {
        determinant = StrToFloat(Form1 -> matrix1 -> Cells[0][0])*
            StrToFloat(Form1 -> matrix1 -> Cells[1][1]) -
            StrToFloat(Form1 -> matrix1 -> Cells[1][0])*
            StrToFloat(Form1 -> matrix1 -> Cells[0][1]);
    }
    else
    {
        double delta;
        determinant = 1;

        for ( int i = 0; i < rowCount1; i++ )
        {
            for ( int j = i+1; j < rowCount1; j++ )
            {
                if ( StrToFloat(Form1 -> matrix1 -> Cells[i][i]) == 0 )
                {
                    if ( StrToFloat(Form1 -> matrix1 -> Cells[i][j]) == 0 )
                        delta = 0;
                }
                else
                {
                    double a = StrToFloat(Form1 -> matrix1 -> Cells[j][i]);
                    double b = StrToFloat(Form1 -> matrix1 -> Cells[i][i]);
                    delta = a / b;
                }

                for ( int k = i; k < rowCount1; k++ )
                {
                    int a = StrToFloat(Form1 -> matrix1 -> Cells[j][k]);
                    int b = StrToFloat(Form1 -> matrix1 -> Cells[i][k]);
                    Form1 -> matrix1 -> Cells[j][k] = a - b*delta;
                }
            }
            determinant = determinant * StrToFloat(Form1 -> matrix1 -> Cells[i][i]);
        }
    }

    return determinant;
}

bool checkMatrix1()
{
    for (int i = 0; i < rowCount1; i++)
    {
        for (int j = 0; j < colCount1; j++)
        {
            if (Form1 -> matrix1 -> Cells[j][i] == "" )
            {
                Dialogs::ShowMessage("Присутствуют пустые клетки.");
                return false;
            }
        }
    }
    return true;
}

bool checkMatrix2()
{
    for (int i = 0; i < rowCount2; i++)
    {
        for (int j = 0; j < colCount2; j++)
        {
            if (Form1 -> matrix2 -> Cells[j][i] == "")
            {
                Dialogs::ShowMessage("Присутствуют пустые клетки.");
                return false;
            }
        }
    }
    return true;
}

void matrixAddition()
{
    double a, b, c;
    rowCount3 = rowCount1;
    colCount3 = colCount1;
    flushMatrix();

    for (int i = 0; i < rowCount1; i++)
    {
        for (int j = 0; j < colCount1; j++)
        {
            a = StrToInt(Form1 -> matrix1 -> Cells[j][i]);
            b = StrToInt(Form1 -> matrix2 -> Cells[j][i]);
            c = a + b;
            Form1 -> matrixResult -> Cells[j][i] = c;
        }
    }
}

void matrixSubstraction()
{
    double a, b, c;
    rowCount3 = rowCount1;
    colCount3 = colCount1;
    flushMatrix();

    for (int i = 0; i < rowCount1; i++)
    {
        for (int j = 0; j < colCount1; j++)
        {
            a = StrToInt(Form1 -> matrix1 -> Cells[j][i]);
            b = StrToInt(Form1 -> matrix2 -> Cells[j][i]);
            c = a - b;
            Form1 -> matrixResult -> Cells[j][i] = c;
        }
    }
}

void matrixMultiplication()
{
    double a, b, c = 0;
    rowCount3 = rowCount1;
    colCount3 = colCount2;
    flushMatrix();

    for(int i = 0; i < rowCount1; i++)
    {
        for(int j = 0; j < colCount2; j++)
        {
            Form1 -> matrixResult -> Cells[j][i] = 0;
            for(int k = 0; k < colCount1; k++)
            {
                a = StrToInt(Form1 -> matrix1 -> Cells[k][i]);
                b = StrToInt(Form1 -> matrix2 -> Cells[j][k]);
                c += (a * b);
                Form1 -> matrixResult -> Cells[j][i] = c;
            }
        }
    }
}


