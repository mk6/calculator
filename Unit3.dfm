object Help: THelp
  Left = 0
  Top = 0
  Caption = #1055#1086#1084#1086#1097#1100
  ClientHeight = 273
  ClientWidth = 436
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object StaticText1: TStaticText
    Left = 168
    Top = 23
    Width = 95
    Height = 17
    Caption = #1043#1086#1088#1103#1095#1080#1077' '#1082#1083#1072#1074#1080#1096#1099
    TabOrder = 0
  end
  object StaticText2: TStaticText
    Left = 0
    Top = 47
    Width = 90
    Height = 17
    Caption = 'C - '#1054#1095#1080#1089#1090#1080#1090#1100' '#1074#1089#1105
    TabOrder = 1
  end
  object StaticText3: TStaticText
    Left = 0
    Top = 63
    Width = 157
    Height = 17
    Caption = 'Backspace - '#1054#1095#1080#1089#1090#1080#1090#1100' '#1101#1083#1077#1084#1077#1085#1090
    TabOrder = 2
  end
  object StaticText4: TStaticText
    Left = 0
    Top = 79
    Width = 180
    Height = 17
    Caption = 'S - '#1042#1099#1095#1080#1089#1083#1080#1090#1100' '#1082#1074#1072#1076#1088#1072#1090#1085#1099#1081' '#1082#1086#1088#1077#1085#1100
    TabOrder = 3
  end
  object StaticText5: TStaticText
    Left = 0
    Top = 95
    Width = 179
    Height = 17
    Caption = 'N - '#1042#1086#1079#1074#1077#1076#1077#1085#1080#1077' '#1095#1080#1089#1083#1072' '#1074' -1 '#1089#1090#1077#1087#1077#1085#1100
    TabOrder = 4
  end
  object StaticText6: TStaticText
    Left = 0
    Top = 111
    Width = 117
    Height = 17
    Caption = 'O - '#1057#1084#1077#1085#1072' '#1079#1085#1072#1082#1072' '#1095#1080#1089#1083#1072
    TabOrder = 5
  end
  object StaticText7: TStaticText
    Left = 0
    Top = 127
    Width = 139
    Height = 17
    Caption = 'I - '#1047#1072#1085#1077#1089#1090#1080' '#1095#1080#1089#1083#1086' '#1074' '#1087#1072#1084#1103#1090#1100
    TabOrder = 6
  end
  object StaticText8: TStaticText
    Left = 0
    Top = 143
    Width = 149
    Height = 17
    Caption = 'R - '#1042#1099#1074#1077#1089#1090#1080' '#1095#1080#1089#1083#1086' '#1080#1079' '#1087#1072#1084#1103#1090#1080
    TabOrder = 7
  end
  object StaticText9: TStaticText
    Left = 0
    Top = 159
    Width = 109
    Height = 17
    Caption = 'D - '#1054#1095#1080#1089#1090#1080#1090#1100' '#1087#1072#1084#1103#1090#1100
    TabOrder = 8
  end
  object StaticText10: TStaticText
    Left = 0
    Top = 175
    Width = 210
    Height = 17
    Caption = 'E - '#1042#1086#1079#1074#1077#1089#1090#1080' '#1095#1080#1089#1083#1086' '#1074' '#1091#1082#1072#1079#1072#1085#1085#1091#1102' '#1089#1090#1077#1087#1077#1085#1100
    TabOrder = 9
  end
  object StaticText11: TStaticText
    Left = 0
    Top = 191
    Width = 66
    Height = 17
    Caption = 'F1 - '#1055#1086#1084#1086#1097#1100
    TabOrder = 10
  end
  object StaticText12: TStaticText
    Left = 0
    Top = 0
    Width = 419
    Height = 17
    Caption = 
      #1063#1090#1086#1073#1099' '#1074#1086#1079#1074#1077#1089#1090#1080' '#1095#1080#1089#1083#1086' '#1074' '#1089#1090#1077#1087#1077#1085#1100', '#1089#1085#1072#1095#1072#1083#1072' '#1091#1082#1072#1078#1080#1090#1077' '#1077#1075#1086' '#1074' '#1089#1086#1086#1090#1074#1077#1090#1089#1090#1074 +
      #1091#1102#1097#1077#1084' '#1087#1086#1083#1077
    TabOrder = 11
  end
  object StaticText13: TStaticText
    Left = 0
    Top = 208
    Width = 90
    Height = 17
    Caption = 'F2 - '#1054' '#1087#1088#1086#1075#1088#1072#1084#1084#1077
    TabOrder = 12
  end
  object StaticText14: TStaticText
    Left = 0
    Top = 224
    Width = 179
    Height = 17
    Caption = 'F3 - '#1058#1088#1080#1075#1086#1085#1086#1084#1077#1090#1088#1080#1095#1077#1089#1082#1080#1077' '#1092#1091#1085#1082#1094#1080#1080
    TabOrder = 13
  end
  object StaticText15: TStaticText
    Left = 0
    Top = 239
    Width = 153
    Height = 17
    Caption = 'F4 - '#1052#1072#1090#1088#1080#1095#1085#1099#1081' '#1082#1072#1083#1100#1082#1091#1083#1103#1090#1086#1088
    TabOrder = 14
  end
end
